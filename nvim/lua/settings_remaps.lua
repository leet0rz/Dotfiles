-- █▀▀ █▀▀ █▄░█ █▀▀ █▀█ ▄▀█ █░░
-- █▄█ ██▄ █░▀█ ██▄ █▀▄ █▀█ █▄▄
-- local remap = vim.keymap.set
local rm = vim.keymap.set
local vc = vim.cmd
local vg = vim.g
local vo = vim.o

-- Title Settings
vo.title = true
vo.titlestring = "%t"

-- Filetype and Plugin Settings
vc('filetype plugin on')
vc('filetype indent on')
-- vc("set autoindent!")
vc("set noswapfile")

-- Display Settings
vo.nu = true
vo.list = true
vim.opt.listchars = { leadmultispace = "│   " }
vo.background = "dark"
vo.breakindent = true
vo.cursorline = true
vo.expandtab = true
vo.lazyredraw = true
vo.mouse = "a"
vo.ruler = true
vo.showcmd = true
vo.syntax = "enable"
vo.timeoutlen = 300
vo.wildmenu = true
vo.wrap = false
vo.completeopt = 'menuone,preview,noselect'

-- Update and Formatting Settings
vo.updatetime = 50
vo.shiftwidth = 4
vo.softtabstop = 4
vo.smarttab = true
vo.tabstop = 4

-- Miscellaneous Settings
vo.autoindent = true
vim.opt.termguicolors = true
vo.clipboard = 'unnamedplus'
vo.encoding = 'utf-8'
vo.hlsearch = true
vo.ignorecase = true
vo.incsearch = true
vo.showmatch = true
vo.smartcase = true
vo.smartindent = true
vo.scrolloff = 5
-- 0==============================================================================================================0
-- 0==============================================================================================================0
-- REMAPS/BINDS/CONFIG:
-- 0==============================================================================================================0
vim.g.mapleader = ' '
-- save file
rm('n', "<C-s>", ":w!<CR>")
-- exit file
rm("n", "<C-q>", ":q!<CR>")
-- alt BS as ctrl BS(backspace)
-- vc('inoremap <M-BS> <C-w>')
rm('i', '<M-BS>', '<C-w>')
-- movement on line, jump, word
rm('n', '<C-Right>', 'w')
rm('n', '<C-Left>', 'b')
rm('v', '<C-Right>', 'w')
rm('v', '<C-Left>', 'b')
rm('i', '<C-Left>', '<C-o>b')
rm('i', '<C-Right>', '<C-o>w')
-- Move selected lines with alt arrows like in subl
rm("v", "<A-Up>", ":m '<-2<CR>gv=gv")
rm("v", "<A-Down>", ":m '>+1<CR>gv=gv")
rm("n", "<A-Up>", ":m .-2<cr>==")
rm("n", "<A-Down>", ":m .+1<cr>==")
-- vertical split
rm('n', '<leader>+', ':vsplit<CR>')
-- hori split
rm('n', '<leader>-', ':split<CR>')
-- Move to splits using leader + arrow keys
rm('n', '<leader><Left>', ':wincmd h<CR>')
rm('n', '<leader><Down>', ':wincmd j<CR>')
rm('n', '<leader><Up>', ':wincmd k<CR>')
rm('n', '<leader><Right>', ':wincmd l<CR>')

rm('t', '<leader><Left>', ':wincmd h<CR>')
rm('t', '<leader><Down>', ':wincmd j<CR>')
rm('t', '<leader><Up>', ':wincmd k<CR>')
rm('t', '<leader><Right>', ':wincmd l<CR>')

-- Indent/Unindent selected text with Tab and Shift+Tab
rm('v', '<Tab>', '>gv')
rm('v', '<S-Tab>', '<gv')
-- search centering
rm('n', 'n', 'nzz')
rm('n', 'N', 'Nzz')
-- movement centering
rm('n', '<C-u>', '<C-u>zz')
rm('n', '<C-d>', '<C-d>zz')
rm('n', '<PageUp>', '<C-u>zz')
rm('n', '<PageDown>', '<C-d>zz')
-- Remove search HL
rm('n', '<leader>h', ':nohlsearch<CR>')
-- change/delete inside parent
rm('n', '<leader>cp', 'cib')
rm('n', '<leader>dp', 'dib')

-- TABS/BUFFERS:
-- Map Ctrl+t to open a new tab:
rm('n', '<C-t>', ':tabnew<CR>')
-- -- next tab
rm('n', '<Tab>', 'gt')
-- -- previous tab
rm('n', '<S-Tab>', 'gT')
-- Close the current tab/buffer:
rm('n', '<leader>q', ':bd!<CR>')
-- next buffer
rm('n', '<A-t>', ':bnext<CR>')

-- S&R -> takes current word
rm('n', '<leader>s', [[:%s/<C-r><C-w>//gc<Left><Left><Left>]])
-- S&R -> takes current selection
rm('v', '<leader>s', [[y:%s/<C-r>"//gc<Left><Left><Left>]])
-- S&R -> select text, press and write what to search for and replace
rm('v', '<leader>sr', [[:s///gI<Left><Left><Left><Left>]])
-- S&R -> in selected field
-- rm('v', '<leader>sr', [[:%s///gI<Left><Left><Left>]])

-- Black formatting
rm('n', '<leader>f', ':!Black %<CR>')
-- Enable arrow key navigation in command-line completion
vc('cnoremap <expr> <Up>   pumvisible() ? "<C-p>" : "<Up>"')
vc('cnoremap <expr> <Down> pumvisible() ? "<C-n>" : "<Down>"')
-- Delete to void?
rm('v', 'd', '"_d')
rm('n', 'd', '"_d')
rm('n', 'x', '"_x')
rm('v', 'x', '"_x')
rm('n', 'c', '"_c')
rm('v', 'c', '"_c')
rm('n', 'p', 'p==')


-- TEST:
-- Don't move cursor when using J to join lines
rm('n', 'J', 'mzJ`z')
rm('n', '<leader>l', ':Lazy<CR>')
rm("v", "<leader>so", ":sort<CR>")

rm('n', '<S-Left>', ':vertical resize -2<CR>')
rm('n', '<S-Right>', ':vertical resize +2<CR>')
rm('n', '<S-Up>', ':resize -2<CR>')
rm('n', '<S-Down>', ':resize +2<CR>')

M = {}
function M.plugin_remaps()
    --TELESCOPE
    builtin = require('telescope.builtin')
    rm('n', '<leader>p', builtin.find_files, {})
    rm('n', '<leader>fg', builtin.live_grep, {})
    rm('n', '<leader>fb', builtin.buffers, {})
    rm('n', '<leader>fh', builtin.help_tags, {})
    -- TS LSP
    --TELESCOPE FILE BROWSER
    -- rm("n", "<C-b>", ":Telescope file_browser path=%:p:h select_buffer=true<CR>")
    -- OIL
    rm("n", "<leader>o", ":e .<CR>")
    rm("n", "<C-b>", ":e .<CR>")
    -- rm("n", "<C-b>", "<cmd>require('oil').open_float()<CR>")
    -- rm("n", "<C-b>", ":Oil --float<CR>")
    -- rm("n", "<leader>o", ":Oil --float<CR>")
    -- rm("n", "<leader>o", ":Oil<CR>")
    -- UNDO TREE
    rm('n', '<leader><F5>', vim.cmd.UndotreeToggle)
    -- CODERUNNER
    rm('n', '<leader>n', ':RunFile<CR>:wincmd k<CR>')
    rm('n', '<leader><leader>', ':RunClose<CR>')
    -- CODEIUM
    -- rm('i', '<c-.>', function() return vim.fn['codeium#CycleCompletions'](-1) end, { expr = true })
    --NVIM-COMMENT
    rm('n', "'", ':CommentToggle<CR>')
    rm('v', "'", ':CommentToggle<CR>')
    -- Toggleterm
    -- rm('n', "<leader>t", ':ToggleTerm direction=float<CR>')
    -- rm('n', "<leader>t", ':ToggleTerm <CR>')
    -- rm('t', "<leader>t", '<Cmd>ToggleTerm <CR>')
    --
    -- rm('n', '<A-Left>', [[<Cmd>wincmd h<CR>]])
    -- rm('t', '<A-Left>', [[<Cmd>wincmd h<CR>]])
    -- rm('n', '<A-Right>', [[<Cmd>wincmd l<CR>]])
    -- rm('t', '<A-Right>', [[<Cmd>wincmd l<CR>]])

    -- rm({"n", "i"}, "<C-S-Up>", "<Cmd>MultipleCursorsAddUp<CR>")
    -- rm({"n", "i"}, "<C-S-Down>", "<Cmd>MultipleCursorsAddDown<CR>")
    -- rm({"n", "i"}, "<C-k>", "<Cmd>MultipleCursorsAddUp<CR>")
    -- rm({"n", "i"}, "<C-j>", "<Cmd>MultipleCursorsAddDown<CR>")
    rm({"n", "i"}, "<A-LeftMouse>", "<Cmd>MultipleCursorsMouseAddDelete<CR>")


    --LSP
    rm('n', '<leader>gd', vim.lsp.buf.definition, { desc = "[G]oto [D]efinition" })
    rm('n', '<leader>ga', vim.lsp.buf.code_action, { desc = "[G]oto [A]ction" })
    rm('n', '<leader>sdd', vim.diagnostic.setloclist, { desc = "[S]earch [D]iagnostics" })
    rm('n', '<leader>sd', "<cmd>Telescope diagnostics severity_bound=ERROR <CR>", { desc = '[S]earch [D]iagnostics' })
    -- rm('n', '<leader>ge', vim.diagnostic.open_float)
    -- rm("n", "gr", vim.lsp.buf.rename)
    -- K is lsp hover key
end

return M

-- 0==============================================================================================================0
-- 0==============================================================================================================0
-- 0==============================================================================================================0
-- NVIM BINDINGS
-- 0==============================================================================================================0
-- undo/redo
-- u/ctrl + r
-- search and replace selected
-- :s/SEARCHWORD/REPLACEWORD <ENTER>
-- search and replace global:
-- :s%/SEARCHWORD/REPLACEWORD/gI <ENTER>
-- change in quotes or function:
-- ci" or ci-( or cit for change in tags
-- V select entire line
--
-- visual block mode:
-- ctrl + alt + v
-- change word under cursor
-- cw / ce
-- Change upper/lower case:
-- U - selected text to uppercase
-- u - selected text to lowercase
--
-- gd - jump to definition
-- ctrl+o - jump back again after gd or just in general
--
--
-- RECORDING:
-- q + [a-z] [ALL ACTIONS] q
-- press @ + letter to activate macro. press @@ to run it again.
-- :! <- interacts with terminal
-- 0==============================================================================================================0
-- KEY LIST:
-- <BS>           Backspace
-- <BS>           Backspace
-- <Tab>          Tab
-- <CR>           Enter
-- <Enter>        Enter
-- <Return>       Enter
-- <Esc>          Escape
-- <Space>        Space
-- <Up>           Up arrow
-- <Down>         Down arrow
-- <Left>         Left arrow
-- <Right>        Right arrow
-- <F1> - <F12>   Function keys 1 to 12
-- #1, #2..#9,#0  Function keys F1 to F9, F10
-- <Insert>       Insert
-- <Del>          Delete
-- <Home>         Home
-- <End>          End
-- <PageUp>       Page-Up
-- <PageDown>     Page-Down
-- <bar>          the '|' character, which otherwise needs to be escaped '\|'
-- <C>

-- 0==============================================================================================================0
-- 0==============================================================================================================0
-- BINDINGS OF PLUGINS:
-- 0==============================================================================================================0
-- telescope:
-- <C-n>/<Down> 	Next item
-- <C-p>/<Up> 	    Previous item
-- j/k              Next/previous (in normal mode)
-- H/M/L            Select High/Middle/Low (in normal mode)
-- gg/G             Select the first/last item (in normal mode)
-- <CR>             Confirm selection
-- <C-x>            Go to file selection as a split
-- <C-v>            Go to file selection as a vsplit
-- <C-t>            Go to a file in a new tab
-- <C-u>            Scroll up in preview window ---
-- <C-d>            Scroll down in preview window ---
-- <C-f>            Scroll left in preview window
-- <C-k>            Scroll right in preview window
-- <M-f>            Scroll left in results window
-- <M-k>            Scroll right in results window
-- <C-/>            Show mappings for picker actions (insert mode)
-- ?                Show mappings for picker actions (normal mode)
-- <C-c>            Close telescope
-- <Esc>            Close telescope (in normal mode)
-- <Tab>            Toggle selection and move to next selection
-- <S-Tab>          Toggle selection and move to prev selection
-- <C-q>            Send all items not filtered to quickfixlist (qflist)
-- <M-q>            Send all selected items to qflist

-- 0==============================================================================================================0
-- Oil.nvim -> keymaps changeable in the oil config.
-- ["g?"] = "actions.show_help",
-- ["<CR>"] = "actions.select",
-- ["<C-s>"] = "actions.select_vsplit",
-- ["<C-h>"] = "actions.select_split",
-- ["<C-t>"] = "actions.select_tab",
-- ["<C-p>"] = "actions.preview",
-- ["<C-c>"] = "actions.close",
-- ["<C-l>"] = "actions.refresh",
-- ["<BS>"] = "actions.parent",
-- ["_"] = "actions.open_cwd",
-- ["`"] = "actions.cd",
-- ["~"] = "actions.tcd",
-- ["gs"] = "actions.change_sort",
-- ["g."] = "actions.toggle_hidden",

-- 0==============================================================================================================0
-- LEAP
-- s + 2 letters of the word you want to jump to. Then check the labels on the words already marked by leap and press the label to go there.
--
-- 0==============================================================================================================0
-- telescope filebrowser:
-- <A-c>/c 	create 	            Create file/folder at current path (trailing path separator creates folder)
-- <S-CR> 	create_from_prompt 	Create and open file/folder from prompt (trailing path separator creates folder)
-- <A-r>/r 	rename 	            Rename multi-selected files/folders
-- <A-m>/m 	move                Move multi-selected files/folders to current path
-- <A-y>/y 	copy                Copy (multi-)selected files/folders to current path
-- <A-d>/d 	remove              Delete (multi-)selected files/folders
-- <C-o>/o 	open                Open file/folder with default system application
-- <C-g>/g 	goto_parent_dir 	Go to parent directory
-- <C-e>/e 	goto_home_dir 	    Go to home directory
-- <C-w>/w 	goto_cwd 	        Go to current working directory (cwd)
-- <C-t>/t 	change_cwd 	        Change nvim's cwd to selected folder/file(parent)
-- <C-f>/f 	toggle_browser 	    Toggle between file and folder browser
-- <C-h>/h 	toggle_hidden 	    Toggle hidden files/folders
-- <C-s>/s 	toggle_all 	        Toggle all entries ignoring ./ and ../
-- <Tab> 	see telescope.nvim 	Toggle selection and move to next selection
-- <S-Tab> 	see telescope.nvim 	Toggle selection and move to prev selection
-- <bs>/ 	backspace 	        With an empty prompt, goes to parent dir. Otherwise acts normally







-- 0==============================================================================================================0
-- BACKUP
--:vc(':nnoremap <C-l> <C-w>')

-- rm('i', '<C-Backspace>', '<C-w>')
-- vim.keymap.set('i', '<C-BS>', '<C-w>')
-- vim.keymap.set({ 'i', 'c' }, '<C-BS>', '<C-w>')

-- file tree open/close
-- vc(':nnoremap <C-b> :NvimTreeToggle<CR>')

-- vc(':nnoremap <C-s> :w<CR>')
-- vc(':nnoremap <C-q> :q!<CR>')
--Map Shift+Tab to switch to the previous buffer:
-- rm('n', '<S-Tab>', ':bprevious<CR>')

-- nvim tree keys:
-- a - create file
-- d - delete file/folder
-- enter - open file/folder
-- C - copy
-- p - paste
-- e - rename
-- q - close
--
-- Backspace - close dir
-- tab - open preview
-- - and + - up and down a dir
-- R - refresh
-- gy copy absolute path
-- <C-K> - info
-- D - Trash
-- S - search

--Map Tab to switch to the next buffer:
-- rm('n', '<Tab>', ':bnext<CR>')

--NVIM-TREE
--vc(':nnoremap <C-b> :NvimTreeToggle<CR>')
-- rm('n', '<leader>sr', [[:%s///gI<Left><Left><Left><Left>]])
-- rm('v', '<leader>sr', [[:%s/\<<C-r><C-w>\>//gI<Left><Left><Left>]])
