filetype = vim.bo.filetype
function pather()
    -- pre
    fullpath = vim.fn.expand("%:p")
    filetype = vim.bo.filetype
   --  get_mode = vim.api.nvim_get_mode().mode
    fullpath_len = string.len(fullpath) 

    -- if get_mode == "t" then
    --     return ""
    if string.find(fullpath, "\\") then
        firstpath = string.sub(fullpath, 1, 3)
        firstpath = string.gsub(firstpath, ":", "")
        secondpath = string.sub(fullpath, 4, -1)
        fullpath = "" .. firstpath .. "" .. secondpath
        replacement = string.gsub(fullpath, "\\", "📁")
        -- replacement = string.gsub(fullpath, "\\", " > ")
        return replacement
    elseif string.find(fullpath, "/") then
        fullpath = string.sub(fullpath, 8, -1)
        replacement = string.gsub(fullpath, "/", "📁")
        -- replacement = string.gsub(fullpath, "/", " > ")
        return replacement
    else
        return fullpath
    end

end

local function pather2()
    local fullpath = vim.fn.expand("%:p")
    if string.find(fullpath, "oil:") then
        fullpath = string.sub(fullpath, 8, -1)
        return fullpath
    else 
        return fullpath
    end
end

-- function top_line()
--     window_width = vim.fn.winwidth(0)
--     return string.rep("─", window_width - fullpath_len - 12)
-- end

function top_line2()
    pather2_length = string.len(pather2())
    -- window_width = vim.fn.winwidth(0)
    window_width = vim.api.nvim_win_get_width(0)
    return string.rep("—", window_width - pather2_length - 5)
end

local remove_these = {
    -- "help",
    "terminal",
    "toggleterm",
    "command line",
    "[Command Line]",
}


function winbar_render()
    -- local winbar_colorpick = "aqua"
    -- if vim.cmd("colorscheme") == "moonlight" then
    local winbar_colorpick = "darkaqua"
    -- elseif vim.cmd("colorscheme") == "carbonfox" then
    -- local winbar_colorpick = "grey"
    -- end
    -- local winbar_colorpick = "darkwhite"

    if vim.bo.buftype == "terminal" then
            return ""
    elseif string.find(vim.fn.expand("%:p"), "Command") then
        return ""
    else
        return table.concat({
            color_string("winbarleftside", winbar_colorpick, "┌["),
            -- " ",
            color_string("pather2", winbar_colorpick, pather2()),
            color_string("winbarrightside", winbar_colorpick, "]"),
            -- " ",
            color_string("topline2", winbar_colorpick, top_line2()),
        })
    end
end

vim.opt.winbar = "  %{%v:lua.winbar_render()%}"



