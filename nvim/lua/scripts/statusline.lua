-- 0=============================================================================================0
-- █▀ ▀█▀ ▄▀█ ▀█▀ █░█ █▀ █░░ █ █▄░█ █▀▀
-- ▄█ ░█░ █▀█ ░█░ █▄█ ▄█ █▄▄ █ █░▀█ ██▄
-- 0=============================================================================================0
-- disable modes showing in native vim.
vim.o.showmode = false
-- 0=============================================================================================0
-- Get color from tables.lua, hl_colors is a table of colors and color codes.
function get_hl_color(color)
    return hl_colors[color]
end


-- 0=============================================================================================0
-- GETS MODE FROM VIM API RETURNED AS STRING
local function show_mode()
    get_mode = vim.api.nvim_get_mode().mode
    local mode_name = mode_convert1[get_mode]
    if mode_name == "NORMAL" then
        return  color_string2("normalmode", "black", "navyblue", " ＮＯＲＭＡＬ ")
    elseif mode_name == "INSERT" then
        return color_string2("insertmode", "black", "green3", " ＩＮＳＥＲＴ ")
    elseif mode_name == "VISUAL" then
        return color_string2("visualmode", "black", "purple", " ＶＩＳＵＡＬ ")
    elseif mode_name == "V-BLOCK" then
        return color_string2("visualblockmode", "black", "purple", " Ｖ－ＢＬＯＣＫ ")
    elseif mode_name == "REPLACE" then
        return color_string2("replacemode", "black", "red", " ＲＥＰＬＡＣＥ ")
    elseif mode_name == "COMMAND" then
        return color_string2("comandmode", "black", "yellow2", " ＣＯＭＭＡＮＤ ")
    elseif mode_name == "TERMINAL" then
        return color_string2("terminalmode", "black", "yellow2", " ＴＥＲＭ ")
    else
        return get_mode
    end
end

local function show_mode_spacer()
    get_mode = vim.api.nvim_get_mode().mode
    local mode_name = mode_convert1[get_mode]
    if mode_name == "NORMAL" then
        return  color_string2("normalmode", "black", "navyblue", "  ")
    elseif mode_name == "INSERT" then
        return color_string2("insertmode", "black", "green3", "  ")
    elseif mode_name == "VISUAL" then
        return color_string2("visualmode", "black", "purple", "  ")
    elseif mode_name == "V-BLOCK" then
        return color_string2("visualblockmode", "black", "purple", "  ")
    elseif mode_name == "REPLACE" then
        return color_string2("replacemode", "black", "red", "  ")
    elseif mode_name == "COMMAND" then
        return color_string2("comandmode", "black", "yellow2", "  ")
    elseif mode_name == "TERMINAL" then
        return color_string2("terminalmode", "black", "yellow2", "  ")
    else
        return get_mode
    end
end

-- write a function to check how many buffers currently open 
local function how_many_buffers()
    return "B:" .. #vim.fn.getbufinfo({ listed = 1 })
end

local function show_diagnostics()
    local bufnr = vim.api.nvim_get_current_buf()
    local diagnostics = vim.diagnostic.get(bufnr)
    local count = vim.tbl_count(vim.tbl_filter(function(diagnostic)
        return diagnostic.severity == vim.diagnostic.severity.ERROR
    end, diagnostics))
    -- return count > 0 and "E:" .. count or ""
    return count > 0 and "-" .. count or ""
end

local function count_buffers()
    local total_buffers = #vim.api.nvim_list_bufs()
    print("Total buffers:", total_buffers)
    return total_buffers
end

-- 0=============================================================================================0
-- CURRENT PATH
local function current_path()
    -- base function for path:
    path = vim.fn.expand("%:p")
    filetype = vim.bo.filetype
    -- logic for shorter path:
    if filetype == "oil" then
        return path
    elseif string.len(path) > 40 then
        -- FIND PATH SEPARATORS
        t1 = {}
        for i = 1, string.len(path) do
            if string.sub(path, i, i) == "\\" or string.sub(path, i, i) == "/" then
                table.insert(t1, i)
            end
        end
        -- Length of t1 as int
        local t1_length = #t1
        -- creates variable of path, path and file.
        -- local t1_final = t1[t1_length - 1]
        local t1_final = t1[t1_length]
        return path:sub(1, 3) .. ".." .. path:sub(t1_final, -1)
    else
        return path
    end
end
-- 0=============================================================================================0
local function statusline_spacer()
    -- return "%#Statusline#%="
    return "%##%="
end

-- 0=============================================================================================0
local function file_name()
    if vim.bo.filetype == "oil" then
        local full_path1 = vim.fn.expand("%:p")
        full_path1 = string.gsub(full_path1, "oil://", " ")
        return full_path1 .. " "
    else

        local x = vim.fn.expand("%:t")
        return " " .. x .. " "
    end
    -- return color_string("white", " " .. x .. " "
end
-- 0=============================================================================================0
-- WIN OR LINUX OR MACOS
local function check_os()
    if package.config:sub(1, 1) == '\\' then
        -- return "  WIN"
        return " "
    else
        return " "
    end
end
-- 0=============================================================================================0
-- FILE TYPE
-- vim.bo.filetype -> RETURNS CURRENT BUFFER FILETYPE AS STRING
local function file_type()
    filetype = vim.bo.filetype
    filetype = string.upper(filetype)
    --new
    if string.find(filetype, "LUA") then
        -- return "🔵" .. filetype
        return " " .. filetype
    elseif string.find(filetype, "PYTHON") then
        -- return "🐍PY"
        return " PY"
    else
    --new 
        return filetype
    end
end
-- 0=============================================================================================0
-- ENCODING -> UTF-8 etc.
local function encoding()
    local encoding = vim.opt.fileencoding:get()
    if encoding == nil then
        encoding = ""
    end
    encoding = string.upper(encoding)
    return tostring(encoding)
end
-- 0=============================================================================================0
local function percent_location()
    -- return "🌎%p%%"
    return "%p%%"
end
-- 0=============================================================================================0
-- Location's percentage in file
local function line_location()
    -- return "🌎 %p%%/%l/%L"
    return "%l/%L"
end
-- 0=============================================================================================0
function getCurrentTime()
    local current_time = os.date("%H:%M")
    return "" .. current_time
end

-- 0=============================================================================================0
-- yellow highlight group
-- local test = "%#COLOR#STRING%##"
-- CREATING THE STRING
local colorpick1 = "darkwhite"
-- local colorpick1 = "white"
local colorpick2 = "black"
-- local colorpick2 = "darkwhite"
function statusline_render()
    if vim.bo.buftype == "terminal" then
        return ""
    else
        return table.concat({
            -- LEFT
            -- MODE
            show_mode(),
            -- filename
            color_string2("file_name", colorpick1, "darkerblue", file_name()),
            -- mode spacer
            show_mode_spacer(),
            color_string2("show_diag", "red", "NONE", " " .. show_diagnostics() .. " "),
            -- BIG SPACER
            -- color_string("statuslinespacer",colorpick1, statusline_spacer()),
            color_string2("statuslinespacer","NONE", "NONE", statusline_spacer()),

            -- RIGHT
            -- OS
            -- Red, orange, yellow, green, blue, indigo, violet
            color_string2("check_os", colorpick2, "softred", " " .. check_os() .. " "),
            -- FILETYPE + EMOJI
            color_string2("file_type", colorpick2, "softorange", " " .. file_type() .. " "),
            -- ENCODING
            -- color_string(colorpick1, encoding()),
            color_string2("encoding", colorpick2, "softyellow", " " .. encoding() .. " "),
            -- PERCENTAGE
            color_string2("percent_location", colorpick2, "softgreen", " " .. percent_location() .. " "),
            -- LINELOCATION
            color_string2("line_location", colorpick2, "softblue2", " " .. line_location() .. " "),

            -- show_mode_spacer(),
            -- TIME
            -- color_string("white", "["),
            -- getCurrentTime(),
            color_string2("getcurrenttime", colorpick2, "softindigo", " " .. getCurrentTime() .. " "),
            -- color_string("white", "]"),
        })
    end
end
-- 0=============================================================================================0
-- FEEDING statusline_render() INTO VIM'S STATUSLINE
vim.opt.statusline = "%{%v:lua.statusline_render()%}"

